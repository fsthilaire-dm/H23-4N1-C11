﻿using SuperCarte.Core.Models;
using SuperCarte.EF.Data;

namespace SuperCarte.Core.Extensions;

/// <summary>
/// Classe statique qui regroupe les méthodes d'extension pour la conversion (mapping) du modèle Utilisateur
/// </summary>
public static class UtilisateurMapExtension
{
    /// <summary>
    /// Convertir un objet Utilisateur vers un objet UtilisateurModel
    /// </summary>
    /// <param name="item">Objet à convertir</param>
    /// <returns>Objet converti</returns>
    public static UtilisateurModel VersUtilisateurModel(this Utilisateur item)
    {
        return new UtilisateurModel()
        {
            UtilisateurId = item.UtilisateurId,
            Prenom = item.Prenom,
            Nom = item.Nom,
            NomUtilisateur = item.NomUtilisateur,            
            RoleId = item.RoleId
        };
    }

    /// <summary>
    /// Convertir un objet UtilisateurModel vers un objet Utilisateur
    /// </summary>
    /// <param name="item">Objet à convertir</param>
    /// <returns>Objet converti</returns>
    public static Utilisateur VersUtilisateur(this UtilisateurModel item)
    {
        return new Utilisateur()
        {
            UtilisateurId = item.UtilisateurId,
            Prenom = item.Prenom,
            Nom = item.Nom,
            NomUtilisateur = item.NomUtilisateur,                      
            RoleId = item.RoleId
        };
    }    

    /// <summary>
    /// Méthode qui copie les valeurs des propriétés de l'objet de donnée Utilisateur dans l'objet du modèle UtilisateurModel
    /// </summary>
    /// <param name="itemDestination">UtilisateurModel à recevoir la copie (destination)</param>
    /// <param name="utilisateurSource">L'objet Utilisateurde référence pour la copie (source)</param>
    /// <param name="copierClePrimaire">Copier de la clé primaire</param>
    public static void Copie(this UtilisateurModel itemDestination, Utilisateur utilisateurSource, bool copierClePrimaire)
    {
        if (copierClePrimaire == true)
        {
            itemDestination.UtilisateurId = utilisateurSource.UtilisateurId;
        }

        itemDestination.Prenom = utilisateurSource.Prenom;
        itemDestination.Nom = utilisateurSource.Nom;
        itemDestination.NomUtilisateur = utilisateurSource.NomUtilisateur;        
        itemDestination.RoleId = utilisateurSource.RoleId;
    }

    /// <summary>
    /// Méthode qui copie les valeurs des propriétés du UtilisateurModel dans l'objet de donnée Utilisateur
    /// </summary>
    /// <param name="itemDestination">Utilisateur à recevoir la copie (destination)</param>
    /// <param name="utilisateurModelSource">L'objet UtilisateurModel de référence pour la copie (source)</param>
    /// <param name="ignoreClePrimaire">Ignore la copie de la clé primaire</param>
    public static void Copie(this Utilisateur itemDestination, UtilisateurModel utilisateurModelSource, bool ignoreClePrimaire = true)
    {
        if (ignoreClePrimaire == false)
        {
            itemDestination.UtilisateurId = utilisateurModelSource.UtilisateurId;
        }

        itemDestination.Prenom = utilisateurModelSource.Prenom;
        itemDestination.Nom = utilisateurModelSource.Nom;
        itemDestination.NomUtilisateur = utilisateurModelSource.NomUtilisateur;        
        itemDestination.RoleId = utilisateurModelSource.RoleId;
    }
}
