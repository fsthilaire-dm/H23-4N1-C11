﻿namespace SuperCarte.Core.Models;

/// <summary>
/// Classe qui contient l'information d'une catégorie
/// </summary>
public class CategorieModel
{
    public int CategorieId { get; set; }

    public string Nom { get; set; } = null!;

    public string? Description { get; set; }
}
