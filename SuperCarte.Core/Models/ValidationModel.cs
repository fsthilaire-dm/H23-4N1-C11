﻿namespace SuperCarte.Core.Models;

/// <summary>
/// Classe qui contient l'information de la validation
/// </summary>
public class ValidationModel
{
    public ValidationModel()
    {
        ErreurParPropriete = new Dictionary<string, string>();
    }

    /// <summary>
    /// Assigner une erreur pour une propriété du modèle
    /// </summary>
    /// <param name="propriete">Nom de la propriété</param>
    /// <param name="erreur">Message d'erreur</param>
    public void AssignerErreur(string propriete, string erreur)
    {
        if(ErreurParPropriete.ContainsKey(propriete) == false)
        {
            ErreurParPropriete.Add(propriete, erreur);
        }
        else
        {
            ErreurParPropriete[propriete] = erreur;
        }
    } 
    
    public Dictionary<string, string> ErreurParPropriete { get; private set; }

    public bool EstValide
    {
        get
        {
            return !ErreurParPropriete.Any();
        }
    }
}
