﻿namespace SuperCarte.Core.Repositories.Bases;

/// <summary>
/// Interface générique qui contient les opérations de base des tables de la base de données pour une table à clé primaire unique
/// </summary>
/// <typeparam name="TData">Type du modèle de données / table</typeparam>
/// <typeparam name="TClePrimaire">Type de la clé primaire</typeparam>
public interface IBasePKUniqueRepo<TData, TClePrimaire> : IBaseRepo<TData> where TData : class
{
    /// <summary>
    /// Obtenir un item spécifique en fonction de sa clé primaire en asynchrone.
    /// </summary>
    /// <param name="clePrimaire">Valeur de la clé primaire</param>
    /// <returns>L'item ou null si non trouvé</returns>
    Task<TData?> ObtenirParCleAsync(TClePrimaire clePrimaire);

    /// <summary>
    /// Obtenir un item spécifique en fonction de sa clé primaire.
    /// </summary>
    /// <param name="clePrimaire">Valeur de la clé primaire</param>
    /// <returns>L'item ou null si non trouvé</returns>
    TData? ObtenirParCle(TClePrimaire clePrimaire);

    /// <summary>
    /// Suprimer un item spécifique en fonction de sa clé primaire en asynchrone.
    /// </summary>
    /// <param name="clePrimaire">Valeur de la clé primaire</param>    
    /// <param name="enregistrer">Enregistrer immédiatement ou non dans la base de données</param>
    Task SupprimerParCleAsync(TClePrimaire clePrimaire, bool enregistrer);

    /// <summary>
    /// Suprimer un item spécifique en fonction de sa clé primaire.
    /// </summary>
    /// <param name="clePrimaire">Valeur de la clé primaire</param>    
    /// <param name="enregistrer">Enregistrer immédiatement ou non dans la base de données</param>
    void SupprimerParCle(TClePrimaire clePrimaire, bool enregistrer);
}