﻿using Microsoft.EntityFrameworkCore;
using SuperCarte.Core.Models;
using SuperCarte.Core.Repositories.Bases;
using SuperCarte.EF.Data;
using SuperCarte.EF.Data.Context;

namespace SuperCarte.Core.Repositories;

/// <summary>
/// Classe qui contient les méthodes de communication avec la base de données pour la table Carte
/// </summary>
public class CarteRepo : BasePKUniqueRepo<Carte, int>, ICarteRepo
{
    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="bd">Contexte de la base de données</param>
    public CarteRepo(SuperCarteContext bd) : base(bd)
    {
        //Vide, il sert uniquement a recevoir le contexte et à l'envoyer à la classe parent.
    }

    public async Task<List<CarteDetailModel>> ObtenirListeCarteDetailAsync()
    {
        return await(from lqCarte in _bd.CarteTb
                     select
                         new CarteDetailModel()
                         {
                             CarteId = lqCarte.CarteId,
                             Nom = lqCarte.Nom,
                             Vie = lqCarte.Vie,
                             Armure = lqCarte.Armure,
                             Attaque = lqCarte.Attaque,
                             EstRare = lqCarte.EstRare,
                             PrixRevente = lqCarte.PrixRevente,
                             CategorieId = lqCarte.CategorieId,
                             CategorieNom = lqCarte.Categorie.Nom,
                             EnsembleId = lqCarte.EnsembleId,
                             EnsembleNom = lqCarte.Ensemble.Nom
                         }).ToListAsync();
    }
}
