﻿using SuperCarte.Core.Models;

namespace SuperCarte.Core.Services;

/// <summary>
/// Interface qui contient les services du modèle Utilisateur
/// </summary>
public interface IUtilisateurService
{
    /// <summary>
    /// Obtenir un utilisateur en partir de sa clé primaire en asynchrone.
    /// </summary>
    /// <param name="utilisateurId">Clé primaire de l'utilisateur</param>
    /// <returns>L'utilisateur ou null si l'utilisateur n'est pas trouvé</returns>
    Task<UtilisateurModel?> ObtenirAsync(int utilisateurId);

    /// <summary>
    /// Obtenir un utilisateur en partir de sa clé primaire.
    /// </summary>
    /// <param name="utilisateurId">Clé primaire de l'utilisateur</param>
    /// <returns>L'utilisateur ou null si l'utilisateur n'est pas trouvé</returns>
    UtilisateurModel? Obtenir(int utilisateurId);

    /// <summary>
    /// Ajouter un utilisateur en asynchrone.
    /// </summary>
    /// <param name="utilisateurModel">Utilisateur à ajouter</param>        
    /// <param name="motPasse">Mot de passe</param>
    /// <returns>Vrai si ajouté, faux si non ajouté</returns>
    Task<bool> AjouterAsync(UtilisateurModel utilisateurModel, string motPasse);

    /// <summary>
    /// Modifier un utilisateur en asynchrone.
    /// </summary>
    /// <param name="utilisateurModel">Utilisateur à modifier</param>
    /// <returns>Vrai si ajouté, faux si non ajouté</returns>
    Task<bool> ModifierAsync(UtilisateurModel utilisateurModel);
}
