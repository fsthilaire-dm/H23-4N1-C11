﻿using SuperCarte.Core.Extensions;
using SuperCarte.Core.Models;
using SuperCarte.Core.Repositories;
using SuperCarte.EF.Data;
using BC = BCrypt.Net.BCrypt; //La classe a le même nom qu'une partie du namespace. Cette nomenclature permet de renommer la classe.

namespace SuperCarte.Core.Services;

/// <summary>
/// Classe qui contient les services du modèle Utilisateur
/// </summary>
public class UtilisateurService : IUtilisateurService
{
    private readonly IUtilisateurRepo _utilisateurRepo;

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="utilisateurRepo">Repository Utilisateur</param>
    public UtilisateurService(IUtilisateurRepo utilisateurRepo)
    {
        _utilisateurRepo = utilisateurRepo;
    }

    public async Task<bool> AjouterAsync(UtilisateurModel utilisateurModel, string motPasse)
    {
        //Transformation de l'objet du modèle du domaine en objet du modèle de données
        Utilisateur utilisateur = utilisateurModel.VersUtilisateur();

        //Le mot de passe n'est pas copié, il faut le convertir avec BCrypt
        utilisateur.MotPasseHash = BC.HashPassword(motPasse);

        //Ajout dans repository avec enregistrement immédiat
        await _utilisateurRepo.AjouterAsync(utilisateur, true);

        //Assigne les valeurs de la base de données dans l'objet du modèle
        utilisateurModel.Copie(utilisateur, true);

        return true;
    }

    public async Task<bool> ModifierAsync(UtilisateurModel utilisateurModel)
    {
        //Il n'y a aucune référence au mot de passe.
        Utilisateur? utilisateur = await _utilisateurRepo.ObtenirParCleAsync(utilisateurModel.UtilisateurId);

        if (utilisateur != null)
        {
            //Assigner les valeurs dans l'utilisateur, sauf pour le mot de passe.
            utilisateur.Copie(utilisateurModel);

            await _utilisateurRepo.EnregistrerAsync();

            //Assigne les valeurs de la base de données dans l'objet du modèle
            utilisateurModel.Copie(utilisateur, false);

            return true;
        }
        else
        {
            throw new Exception("Impossible de modifier l'utilisateur. Aucun utilisateur trouvé avec la clé primaire.");
        }
    }

    public async Task<UtilisateurModel?> ObtenirAsync(int utilisateurId)
    {
        Utilisateur? utilisateur = await _utilisateurRepo.ObtenirParCleAsync(utilisateurId);
        
        return utilisateur?.VersUtilisateurModel();
    }

    public UtilisateurModel? Obtenir(int utilisateurId)
    {
        Utilisateur? utilisateur = _utilisateurRepo.ObtenirParCle(utilisateurId);

        return utilisateur?.VersUtilisateurModel();
    }
}
