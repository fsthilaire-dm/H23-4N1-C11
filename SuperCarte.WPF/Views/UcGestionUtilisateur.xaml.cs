﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SuperCarte.WPF.Views;
/// <summary>
/// Logique d'interaction pour UcGestionUtilisateur.xaml
/// </summary>
public partial class UcGestionUtilisateur : UserControl
{
    public UcGestionUtilisateur()
    {
        InitializeComponent();
    }

    private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
    {
        if(DataContext is GestionUtilisateurVM)
        {
            GestionUtilisateurVM vm = (GestionUtilisateurVM)DataContext;

            vm.MotPasse = (e.Source as PasswordBox).Password;
        }
    }
}
