﻿using System.Globalization;
using System.Windows;

namespace SuperCarte.WPF;
/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window
{
    public MainWindow(MainWindowVM mainWindowVM)
    {
        InitializeComponent();       

        //Permet de spécifier la langue des composants de la vue
        //Si non spécifié, le format des données utilisera le format en-US
        FrameworkElement.LanguageProperty.OverrideMetadata(typeof(FrameworkElement), 
            new FrameworkPropertyMetadata(System.Windows.Markup.XmlLanguage.GetLanguage(CultureInfo.CurrentUICulture.Name)));
        DataContext = mainWindowVM;
    }
}
