﻿using CommunityToolkit.Mvvm.ComponentModel;

namespace SuperCarte.WPF.ViewModels.Bases;

/// <summary>
/// Classe abstraite pour du View Models avec paramètre
/// </summary>
/// <typeparam name="TParametre">Type du paramètre</typeparam>
public abstract class BaseParametreVM<TParametre> : BaseVM
{
    /// <summary>
    /// Assigner des paramètres au ViewModel
    /// </summary>
    /// <param name="parametre">Paramètre à assigner</param>
    public abstract void AssignerParametre(TParametre parametre);
}
