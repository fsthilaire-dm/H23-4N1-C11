﻿namespace SuperCarte.EF.Data;

public class Categorie
{
    public int CategorieId { get; set; }

    public string Nom { get; set; } = null!;

    public string? Description { get; set; }

    public ICollection<Carte> CarteListe { get; set; } = new List<Carte>();
}