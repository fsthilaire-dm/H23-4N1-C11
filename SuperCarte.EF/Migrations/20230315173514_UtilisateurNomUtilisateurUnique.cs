﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SuperCarte.EF.Migrations
{
    /// <inheritdoc />
    public partial class UtilisateurNomUtilisateurUnique : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Utilisateur_NomUtilisateur",
                table: "Utilisateur",
                column: "NomUtilisateur",
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Utilisateur_NomUtilisateur",
                table: "Utilisateur");
        }
    }
}
