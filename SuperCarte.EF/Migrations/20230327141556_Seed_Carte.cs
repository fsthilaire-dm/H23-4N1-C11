﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace SuperCarte.EF.Migrations
{
    /// <inheritdoc />
    public partial class Seed_Carte : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Categorie",
                columns: new[] { "CategorieId", "Description", "Nom" },
                values: new object[,]
                {
                    { 1, null, "Animaux magiques" },
                    { 2, "Les orcs sont une race de guerrier.", "Orcs" },
                    { 3, "Les mages ont des pouvoirs magiques.", "Mages" }
                });

            migrationBuilder.InsertData(
                table: "Ensemble",
                columns: new[] { "EnsembleId", "Disponibilite", "Nom" },
                values: new object[] { 1, new DateTime(2020, 5, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), "Ensemble de départ" });

            migrationBuilder.InsertData(
                table: "Carte",
                columns: new[] { "CarteId", "Armure", "Attaque", "CategorieId", "EnsembleId", "EstRare", "Nom", "PrixRevente", "Vie" },
                values: new object[,]
                {
                    { 1, (short)0, (short)2, 1, 1, false, "Lion des marais", 0.02m, (short)12 },
                    { 2, (short)0, (short)12, 1, 1, true, "Corbeau vampire", 1.20m, (short)2 },
                    { 3, (short)5, (short)5, 2, 1, false, "Grunty", 0.20m, (short)25 }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Carte",
                keyColumn: "CarteId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Carte",
                keyColumn: "CarteId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Carte",
                keyColumn: "CarteId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Categorie",
                keyColumn: "CategorieId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Categorie",
                keyColumn: "CategorieId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Categorie",
                keyColumn: "CategorieId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Ensemble",
                keyColumn: "EnsembleId",
                keyValue: 1);
        }
    }
}
